(function ($) {
	$('.search-field.typeahead')
			.typeahead({
				name    : 'search',
				remote  : {
					url       : wp_typeahead.ajaxurl + '?action=ajax_search&fn=get_ajax_search&terms=%QUERY',
					beforeSend: function () {
						$('.search-form .form-group .loading').removeClass('hidden');
					},
					filter    : function (parsedResponse) {
						$('.search-form .form-group .loading').addClass('hidden');
						return parsedResponse;
					}
				},
				template: [
					'<a class="clearfix" href="{{url}}"><div class="post-thumb"><img src="{{thumb}}" /></div>',
					'<div class="post-title"><strong>{{value}}</strong></div>',
					'<div class="post-excerpt">{{excerpt}}</div></a>'
				].join(''),
				engine  : Hogan
			})
			.on("typeahead:closed ", function () {
			})
			.keypress(function (e) {
				if (13 == e.which) {
					$(this).parents('form').submit();
					return false;
				}
			});
})(jQuery);